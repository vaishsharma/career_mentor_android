package apps.com.careermentor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void careerMentor(View v){
        Intent slideactivity = new Intent(MainActivity.this, MyInfoActivity.class);
        finish();
        //Bundle bndlanimation =
        //		ActivityOptions.makeCustomAnimation(getApplicationContext(), R.xml.animation, R.xml.animation2).toBundle();
        //startActivity(new Intent(ActivityA.this, ActivityB.class));
        startActivity(slideactivity);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    public void online(View v){
        Intent slideactivity = new Intent(MainActivity.this, OnlineActivity.class);
        finish();
        //Bundle bndlanimation =
        //		ActivityOptions.makeCustomAnimation(getApplicationContext(), R.xml.animation, R.xml.animation2).toBundle();
        //startActivity(new Intent(ActivityA.this, ActivityB.class));
        startActivity(slideactivity);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    public void guide(View v){
        Intent slideactivity = new Intent(MainActivity.this, GuideActivity.class);
        finish();
        //Bundle bndlanimation =
        //		ActivityOptions.makeCustomAnimation(getApplicationContext(), R.xml.animation, R.xml.animation2).toBundle();
        //startActivity(new Intent(ActivityA.this, ActivityB.class));
        startActivity(slideactivity);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }
    public void myInfo(View v){
        Intent slideactivity = new Intent(MainActivity.this, CareerEngagementtActivity.class);
        finish();
        //Bundle bndlanimation =
        //		ActivityOptions.makeCustomAnimation(getApplicationContext(), R.xml.animation, R.xml.animation2).toBundle();
        //startActivity(new Intent(ActivityA.this, ActivityB.class));
        startActivity(slideactivity);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
