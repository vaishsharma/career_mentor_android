package apps.com.careermentor;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

/**
 * Created by Admin on 06-Jul-16.
 */
public class OnlineActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online);
    }
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void complete(View v){
        Intent slideactivity = new Intent(OnlineActivity.this, MainActivity.class);
        finish();
        Bundle bndlanimation =
                ActivityOptions.makeCustomAnimation(getApplicationContext(), R.xml.animation, R.xml.animation2).toBundle();
        startActivity(slideactivity, bndlanimation);
    }
}
