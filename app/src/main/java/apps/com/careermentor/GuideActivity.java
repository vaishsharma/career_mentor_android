package apps.com.careermentor;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Admin on 06-Jul-16.
 */
public class GuideActivity extends Activity {

    String s="<b>Website : </b> <a href=www.resources-india.com>www.resources-india.com</a>";
    String s1="You may sometimes need a second opinion. During this session we can <b>review your goals and inherent talents and sync them with your key skills to get outcomes towards career development.</b><br><br>At times you would like to pick up the phone and speak with a trusted coach about how your career is shaping up, what are the things which are driving you crazy at work and what are the challenges you face…. <br>These sessions would offer and edit :<br><br>•  Your professional materials including resumes.<br>•  Cover letters, portfolios, websites, demo reels, artist statements<br>•  Application essays.";
    TextView tv,tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);
        tv= (TextView)findViewById(R.id.txtContactMe);
        tv1= (TextView)findViewById(R.id.txtSecondOpinion);
        tv.setText(Html.fromHtml(s));
        tv1.setText(Html.fromHtml(s1));
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void complete(View v){
        Intent slideactivity = new Intent(GuideActivity.this, MainActivity.class);
        finish();
        Bundle bndlanimation =
                ActivityOptions.makeCustomAnimation(getApplicationContext(), R.xml.animation, R.xml.animation2).toBundle();
        startActivity(slideactivity, bndlanimation);
    }
}
