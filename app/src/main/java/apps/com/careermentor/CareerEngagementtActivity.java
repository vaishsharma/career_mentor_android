package apps.com.careermentor;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class CareerEngagementtActivity extends Activity {

    PieChart pieChart;
    CheckBox chkBoxCvResume,chkBoxCoverLetter,chkBoxInherentTalent,chkBoxJobProfile,chkBoxOnlinePresence,chkBoxTalentAndStrenth,chkBoxUseStrenth;
    ArrayList<Entry> entries;
    String s="Write in for an Express Consult in-confidence to: <a href=strengths@resources-india.com>strengths@resources-india.com</a>";
    String s1="Website: <a href=www.resources-india.com>www.resources-india.com</a>";
    TextView tv,tv1;
    Button btnViewChart;
    ArrayList<String> labels;
    float v1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_career_engagementt);
        tv= (TextView)findViewById(R.id.txtContactMe);
        tv1= (TextView)findViewById(R.id.website);
        pieChart= (PieChart) findViewById(R.id.chart);
        chkBoxCvResume= (CheckBox) findViewById(R.id.chkBoxCvResume);
        chkBoxCoverLetter= (CheckBox) findViewById(R.id.chkBoxCoverLetter);
        chkBoxInherentTalent= (CheckBox) findViewById(R.id.chkBoxInherentTalent);
        chkBoxJobProfile= (CheckBox) findViewById(R.id.chkBoxJobProfile);
        chkBoxOnlinePresence= (CheckBox) findViewById(R.id.chkBoxOnlinePresence);
        chkBoxTalentAndStrenth= (CheckBox) findViewById(R.id.chkBoxTalentAndStrenth);
        chkBoxUseStrenth= (CheckBox) findViewById(R.id.chkBoxUseStrenth);
        btnViewChart= (Button) findViewById(R.id.btnViewChart);
        tv.setText(Html.fromHtml(s));
        tv1.setText(Html.fromHtml(s1));
        //pieChart.setUsePercentValues(true);
        entries = new ArrayList<>();
        labels = new ArrayList<String>();

        btnViewChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pieChart.setVisibility(View.VISIBLE);
                entries.removeAll(entries);
                labels.removeAll(labels);
                v1=0;


                if (chkBoxCvResume.isChecked()) {
                    entries.add(new Entry((int)10, 0));
                    labels.add("A");
                }

                if (chkBoxCoverLetter.isChecked()) {
                    entries.add(new Entry((int)10, 1));
                    labels.add("B");
                }

                if (chkBoxInherentTalent.isChecked()) {
                    entries.add(new Entry((int)20, 2));
                    labels.add("C");
                }

                if (chkBoxJobProfile.isChecked()) {
                    entries.add(new Entry((int)20, 3));
                    labels.add("D");
                }

                if (chkBoxOnlinePresence.isChecked()) {
                    entries.add(new Entry((int)10, 4));
                    labels.add("E");
                }

                if (chkBoxTalentAndStrenth.isChecked()) {
                    entries.add(new Entry((int)20, 5));
                    labels.add("F");
                }

                if (chkBoxUseStrenth.isChecked()) {
                    entries.add(new Entry((int)10, 6));
                    labels.add("G");
                }


                ArrayList<Integer> colors = new ArrayList<Integer>();

                for (int c : ColorTemplate.COLORFUL_COLORS)
                    colors.add(c);

                for (int c : ColorTemplate.PASTEL_COLORS)
                    colors.add(c);

                PieDataSet dataset = new PieDataSet(entries, "");
                PieData data = new PieData(labels, dataset);
                //dataset.setColors(ColorTemplate.COLORFUL_COLORS);
                dataset.setColors(colors);
                Legend l = pieChart.getLegend();
                l.setPosition(Legend.LegendPosition.BELOW_CHART_RIGHT);
                pieChart.setData(data);
                pieChart.setCenterText(String.valueOf((int) dataset.getYValueSum()) + "%");
                pieChart.setCenterTextSize(30);
                pieChart.setCenterTextColor(Color.RED);
                pieChart.animateY(5000);
                pieChart.notifyDataSetChanged();
                pieChart.setDescription("Career Engagement Score");

            }
        });

    }
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void complete(View v){
        Intent slideactivity = new Intent(CareerEngagementtActivity.this, MainActivity.class);
        finish();
        Bundle bndlanimation =
                ActivityOptions.makeCustomAnimation(getApplicationContext(), R.xml.animation, R.xml.animation2).toBundle();
        startActivity(slideactivity, bndlanimation);
    }


}
