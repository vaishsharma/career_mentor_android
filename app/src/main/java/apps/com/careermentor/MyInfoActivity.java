package apps.com.careermentor;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Admin on 06-Jul-16.
 */
public class MyInfoActivity extends Activity {

    String s="Contact me in kind confidence for a career counselling appointment : <br><a href=roma@resources-india.com>roma@resources-india.com</a><br><br>Website : <a href=www.resources-india.com>www.resources-india.com</a>";
    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myinfo);
        tv= (TextView)findViewById(R.id.txtContactMe);
        tv.setText(Html.fromHtml(s));
    }
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void complete(View v){
        Intent slideactivity = new Intent(MyInfoActivity.this, MainActivity.class);
        finish();
        Bundle bndlanimation =
                ActivityOptions.makeCustomAnimation(getApplicationContext(), R.xml.animation, R.xml.animation2).toBundle();
        startActivity(slideactivity, bndlanimation);
    }
}
