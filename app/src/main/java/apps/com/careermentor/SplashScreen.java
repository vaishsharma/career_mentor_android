package apps.com.careermentor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

/**
 * Created by Admin on 06-Jul-16.
 */
public class SplashScreen  extends Activity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity



                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);

        Intent i;

        // if(pref.getUserName() != "" && pref.getCheckBoxValue().equals("unchecked") ) {
        i = new Intent(SplashScreen.this, MainActivity.class);
        startActivity(i);

    }
}
